﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ent.Models
{
    public class FacebookUser
    {
        public long id
        { get; set; }

        public string email
        { get; set; }

        public string name
        { get; set; }

        public string first_name
        { get; set; }

        public string last_name
        { get; set; }

        public string gender
        { get; set; }

        public string link
        { get; set; }

        public DateTime updated_time
        { get; set; }

        public DateTime birthday
        { get; set; }

        public string locale
        { get; set; }

        public Picture picture
        { get; set; }

        public FacebookLocation location
        { get; set; }
    }

    public class FacebookLocation
    {
        public string id
        { get; set; }

        public string name
        { get; set; }
    }
public class Data
{
    public bool is_silhouette { get; set; }
    public string url { get; set; }
}

public class Picture
{
    public Data data { get; set; }
}

}