using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using ent.Models;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Http;
using TweetSharp;
using Tweetinvi.Models;
using Tweetinvi;
// TweetMoaSharp needs this Microsoft.NETCore.Portable.Compatibility 1.0.1

namespace WebApplication.Controllers
{
    public static class MyCredentials
    {
        public static string CONSUMER_KEY = "9m9qUnEJUiuYnQQEt5fKYkcrP";
        public static string CONSUMER_SECRET = "gW34Cf7xfABNzuk36VztdWYD8KoYCpcXyRxUx7yrk0JMRd4Xop";
        public static string ACCESS_TOKEN = "MY_ACCESS_TOKEN";
        public static string ACCESS_TOKEN_SECRET = "MY_ACCESS_TOKEN_SECRET";

        public static ITwitterCredentials GenerateCredentials()
        {
            return new TwitterCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
        }
    }
    public class iss
    {
        public int SrNo { get; set; }
        public int id { get; set; }
        public string title { get; set; }
        public string assignee { get; set; }
        public int timeSpent { get; set; }
        public int timeEst { get; set; }
        public object human_time_estimate { get; set; }
        public object human_total_time_spent { get; set; }
        public string due_date { get; set; }
        public string created_at { get; set; }
        public string Status { get; set; }

    }

    public class ddlList
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
    public class HomeController : Controller
    {
        HttpClient client = new HttpClient();
        private readonly string accesstoken = "yU-zEnG3TbP3jRMRLhT_";
        private readonly GitApiContext DB;

        public HomeController(GitApiContext context)
        {
            DB = context;
        }
        public IActionResult Index()
        {
            if (HttpContext.Session.GetString("UserName") != null)
                return RedirectToAction("IssuesList");
            // HttpContext.Session.Set("q", new List<int>() { 1, 2, 3, 4 });
            // var a = HttpContext.Session.Get<List<int>>("q");
            // HttpContext.Session.Set("q", "2");  
            // var aa = HttpContext.Session.Get<string>("q");
            // HttpContext.Session.Set("q", new List<string> { "vikas", "rai" });
            // var aaa = HttpContext.Session.Get<List<string>>("q");
            return View();
        }
        [HttpPost]
        public ActionResult Index(string User, string Password)
        {
            if (User == "admin" && Password == "m2n1shlko")
            {
                HttpContext.Session.SetString("UserName", User);
                //HttpContext.Session.SetString("Password",Password);
                return RedirectToAction("IssuesList");
            }
            ViewBag.Login = "Login failed";
            return View();

        }
        public ActionResult CallBackFB(string access_token)
        {
            if (access_token == null)
                return Redirect("Index");
            string json = GetFacebookUserJSON(access_token);
            FacebookUser oUser = JsonConvert.DeserializeObject<FacebookUser>(json);
            HttpContext.Session.SetString("UserName", oUser.name);
            HttpContext.Session.SetString("ProfileImageUrl", string.IsNullOrEmpty(oUser.picture.data.url) ? "" : oUser.picture.data.url);
            //HttpContext.Session.SetString("Password",Password);
            return RedirectToAction("IssuesList");
        }

        private static string GetFacebookUserJSON(string access_token)
        {
            string url = string.Format("https://graph.facebook.com/me?access_token={0}&fields=email,name,first_name,last_name,link,picture", access_token);
            string Result = "";
            var client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("").Result;
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                Result = rawResponseContent.Result;
            }

            return Result;
        }
        public ActionResult AuthorizeCallback(string oauth_verifier, string authorization_id)
        {
            var verifierCode = oauth_verifier;
            var authorizationId = authorization_id;

            if (verifierCode != null)
            {
                var userCreds = AuthFlow.CreateCredentialsFromVerifierCode(verifierCode, authorizationId);
                var user = Tweetinvi.User.GetAuthenticatedUser(userCreds);

                HttpContext.Session.SetString("UserName", user.Name);
                HttpContext.Session.SetString("ProfileImageUrl", string.IsNullOrEmpty(user.ProfileImageUrl) ? "" : user.ProfileImageUrl);
                //HttpContext.Session.SetString("Password",Password);
                return RedirectToAction("IssuesList");
            }
            else
                return Redirect("Index");
        }
        public ActionResult Authorize()
        {
            var url = Url.Action("AuthorizeCallback", "Home", null, "http");
            var appCreds = new ConsumerCredentials(MyCredentials.CONSUMER_KEY, MyCredentials.CONSUMER_SECRET);
            var redirectURL = Url.Action("AuthorizeCallback1", "Home", null, "http");
            var authenticationContext = AuthFlow.InitAuthentication(appCreds, url);
             
            return new RedirectResult(authenticationContext.AuthorizationURL);
        }

        public ActionResult LogOut()
        {
            HttpContext.Session.Clear();
            return Redirect("Index");
        }
        public ActionResult IssuesList()
        {
            if (HttpContext.Session.GetString("UserName") == null)
                return RedirectToAction("Index");

            ViewData["ProjectList"] = GetProjects();
            return View();
        }
        public List<ddlList> GetProjects()
        {
            string url = "https://gitlab.com/api/v3/projects/";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
            HttpResponseMessage response = client.GetAsync("").Result;
            List<Projects> Projects = new List<Projects>();
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                Projects = JsonConvert.DeserializeObject<List<Projects>>(rawResponseContent.Result);
            }
            var ProjectList = new List<ddlList>();
            ProjectList = Projects.Select(x => new ddlList() { Text = x.name, Value = x.id.ToString() }).ToList();
            //    ProjectList.Add(new ListItem() { Text = "select Project", Value = "-1",Selected=true });
            return ProjectList;
        }
        public JsonResult GetUsers(string ProjectId)
        {
            string url = "https://gitlab.com/api/v3/projects/" + ProjectId + "/members";
            client = new HttpClient();
            client.BaseAddress = new Uri(url);
            client.DefaultRequestHeaders.Accept.Add(
            new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("PRIVATE-TOKEN", accesstoken);
            HttpResponseMessage response = client.GetAsync("?per_page=100").Result;
            List<Users> Users = new List<Users>();
            if (response.IsSuccessStatusCode)
            {
                var rawResponseContent = response.Content.ReadAsStringAsync();
                Users = JsonConvert.DeserializeObject<List<Users>>(rawResponseContent.Result);
            }
            var UsersList = new List<ddlList>();
            UsersList = Users.Select(x => new ddlList() { Text = x.username, Value = x.id.ToString() }).OrderBy(x => x.Text).ToList();
            return Json(UsersList);
        }
        public JsonResult GetIssues(string ProjectId, string UserId)
        {
            List<iss> Issues = new List<iss>();

            var a = DB.Issues.ToList();

            int i = 1;
            //Issues = dt.AsEnumerable().Select(x => new iss() { SrNo = i++, id = x.Field<int>("Id"), title = x.Field<string>("title"), assignee = x.Field<string>("username"), timeEst = x.Field<int>("time_estimate"), timeSpent = x.Field<int>("total_time_spent"), due_date = x.Field<string>("due_date") == null ? "" : x.Field<string>("due_date"), Status = x.Field<string>("state"), created_at = x.Field<string>("created_at").Substring(0, 10), human_time_estimate = x.Field<string>("human_time_estimate"), human_total_time_spent = x.Field<string>("human_total_time_spent") }).ToList();
            Issues = DB.Issues.Where(x => x.project_id == Convert.ToInt32(ProjectId) && x.assignee.id == Convert.ToInt32(UserId)).Select(x => new iss() { SrNo = (i), id = x.id, title = x.title, assignee = x.assignee.name, timeEst = x.time_estimate, timeSpent = x.total_time_spent, due_date = x.due_date == null ? "" : x.due_date, Status = x.state, created_at = x.created_at.Substring(0, 10), human_time_estimate = x.human_time_estimate, human_total_time_spent = x.human_total_time_spent }).ToList();
            return Json(Issues);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }

    }

}
