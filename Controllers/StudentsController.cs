using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ent.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ent.Controllers
{
    public class StudentsController : Controller
    {
        private readonly SchoolContext _context;

        public StudentsController(SchoolContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
           var a= await _context.Students.ToListAsync();
            return View(a);
        }

        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = await _context.Students
                .Include(s => s.Enrollments)
                    .ThenInclude(e => e.Course)
                .SingleOrDefaultAsync(m => m.ID == id);

            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        public IActionResult Create(){
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Student student)
            {
                try
                {
                    if (ModelState.IsValid)
                    {
                        _context.Add(student);
                        _context.SaveChanges();
                        return Redirect("Index");
                    }
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists " +
                        "see your system administrator.");
                }
                return View(student);
            }

        public IActionResult Edit(int? id){
            if (id ==null)
            {
                return NotFound();
            }
            var student = _context.Students.SingleOrDefault(x=>x.ID==id);
            return View(student);
        }
     
        [HttpPost]
        public IActionResult Edit(int id,Student student)
        {
            if (id != student.ID)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(student);
                    _context.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateException /* ex */)
                {
                    //Log the error (uncomment ex variable name and write a log.)
                    ModelState.AddModelError("", "Unable to save changes. " +
                        "Try again, and if the problem persists, " +
                        "see your system administrator.");
                }
            }
            return View(student);
        }
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var student = _context.Students.SingleOrDefault(x=>x.ID==id);
            if (student == null)
            {
                return NotFound();
            }

            return View(student);
        }

        [HttpPost]
        public IActionResult Delete(int id,string a)
        {
           var student = _context.Students.SingleOrDefault(x=>x.ID==id);
            if (student == null)
            {
                return RedirectToAction("Index");
            }

            try
            {
                _context.Students.Remove(student);
               _context.SaveChanges();
               // TempData["Delete"]="Student Deleted";
                return RedirectToAction("Index");
            }
            catch (DbUpdateException ex )
            {
                TempData["Delete"]="Student not deleted <br> " + ex.Message;
                return RedirectToAction("Delete", new { id = id});
            }
        }
 }

}